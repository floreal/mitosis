package file_test

import (
	"fmt"
	"os"
	"testing"

	"git.framacode.org/floreal/mitosis/fs/file"
	"github.com/stretchr/testify/assert"
)

func TestNew(t *testing.T) {
	a := assert.New(t)

	dirname := "/tmp/test/file"
	e := file.New(dirname)
	a.Equal("/tmp/test/file", e.Path())
	a.Equal("file", e.Name())
}

func TestFile_SetContent(t *testing.T) {
	a := assert.New(t)

	dirname := "/tmp/test/file"
	e := file.New(dirname)
	e.SetContent([]byte(`This is a test`))
	a.Equal(`This is a test`, string(e.Content()))
}

func TestFile_Content(t *testing.T) {
	a := assert.New(t)

	dirname := "/tmp/test/file"
	e := file.New(dirname)
	a.Equal([]byte{}, e.Content())
}

func TestFile_MountAndContent(t *testing.T) {
	a := assert.New(t)

	dirname, _ := os.MkdirTemp("", "test")
	defer os.RemoveAll(dirname)
	filename := fmt.Sprintf(`%s/testfile`, dirname)
	f, _ := os.OpenFile(filename, os.O_CREATE|os.O_WRONLY, 0600)
	c := []byte(`This is a test`)
	f.Write(c)
	_ = f.Close()

	fe := file.New(filename)
	a.Nil(fe.Mount())

	a.Equal(c, fe.Content())
	fe.UnMount()
}

func TestFile_PersistAndMount(t *testing.T) {
	a := assert.New(t)

	dirname, _ := os.MkdirTemp("", "test")
	defer os.RemoveAll(dirname)
	filename := fmt.Sprintf(`%s/testfile`, dirname)

	fe := file.New(filename)
	c := []byte(`This is a test`)
	fe.SetContent(c)
	a.Nil(fe.Persist())
	a.Nil(fe.Persist())

	a.Equal(c, fe.Content())
}

func TestFile_MountFails(t *testing.T) {
	a := assert.New(t)

	dirname := "/tmp/test/file"
	e := file.New(dirname)
	a.EqualError(e.Mount(), `open /tmp/test/file: no such file or directory`)
}
