package file

import (
	"bytes"
	"fmt"
	"io"
	"os"

	nfs "git.framacode.org/floreal/mitosis/fs"
)

const chunkSize int = 65_536 // 64 * 1024

// file is an implementation of
// git.framacode.org/floreal/mitosis/fs/File
// and will respect it's behavior requirement
type file struct {
	name      string
	parent    string
	content   []byte
	isMounted bool
	updated   bool
}

// Mount load the file content in memory.
// from the OS filesystem.
// Mount change the file state to isMounted.
func (f *file) Mount() error {
	file, err := os.OpenFile(f.Path(), os.O_RDONLY, 0700)
	if err != nil {
		return err
	}
	defer file.Close()
	chunk := make([]byte, chunkSize)
	for {
		l, err := file.Read(chunk)

		switch err {
		case nil:
			f.content = append(f.content, chunk[:l]...)
		case io.EOF:
			f.isMounted = true
			return nil
		default:
			return err
		}
	}
}

// UnMount unloads the file content from memory.
// UnMount do NOT write file data to the OS filesystem.
// UnMount changes the file state to !isMounted.
func (f *file) UnMount() {
	f.isMounted = false
}

// Name must provides the file's basename on OS filesystem.
func (f *file) Name() string {
	return f.name
}

// Path must provides the file's fullpath on OS filesystem.
func (f *file) Path() string {
	return fmt.Sprintf(`%s/%s`, f.parent, f.name)
}

// Persists must write file to the OS filesystem.
func (f *file) Persist() error {
	_, err := os.Stat(f.Path())
	if !os.IsNotExist(err) && !f.updated {
		return nil
	}
	file, err := os.OpenFile(f.Path(), os.O_WRONLY|os.O_CREATE, 0700)
	if err != nil {
		return err
	}
	defer file.Close()
	_, err = file.Write(f.content)
	if err == nil {
		f.updated = false
	}
	return err
}

// SetContent must set the File Content.
// The file must be Persisted before being Unmount-ed
func (f *file) SetContent(content []byte) {
	if !bytes.Equal(f.content, content) {
		f.updated = true
		f.content = content
	}
}

// Content must provide the File Content when content is
// changed or when the File is Mound-ed.
func (f *file) Content() []byte {
	return f.content
}

func New(path string) nfs.File {
	parent, name := nfs.ParentAndName(path)
	return &file{
		name:      name,
		parent:    parent,
		content:   []byte{},
		isMounted: false,
	}
}
