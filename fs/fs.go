package fs

import (
	"os"
	"strings"
)

const s string = string(os.PathSeparator)

type EntryType int8

const (
	DirectoryEntry EntryType = iota + 1
	FileEntry
)

// Entry describes how File Regular as well as Directory.
// should behave.
type Entry interface {
	// Mount must load the Entity content in memory.
	// from the OS filesystem.
	// Mount must change the Entity state to isMounted.
	Mount() error

	// UnMount must unload the Entity content from memory.
	// UnMount must NOT write Entity data to the OS filesystem.
	// UnMount must change the Entity state to !isMounted.
	UnMount()

	// Name must provides the Entity's basename on OS filesystem.
	Name() string

	// Path must provides the Entity's fullpath on OS filesystem.
	Path() string

	// Persists must write Entity to the OS filesystem.
	Persist() error
}

// Directory extends Entity and provides more behavior.
// It represents a directory on the OS filesystem.
type Directory interface {
	Entry

	// AddChild must adds An entity to the Children list and
	// return it, with nil as error.
	// AddChild must return error if:
	//  - the current Directory is not Mount-ed
	//  - name is empty (len == 0)
	//  - entryType is invalid (see EntryType)
	AddChild(name string, entryType EntryType) (Entry, error)

	// Children must provide the slice of Children with a nil
	// as error.
	// Children must return error if the current Directory
	// is not Mount-ed.
	Children() ([]Entry, error)
}

// File extends Entity and provide more behavior.
// It represents a regular file on the OS filesystem.
type File interface {
	Entry

	// SetContent must set the File Content.
	// The file must be Persisted before being Unmount-ed
	SetContent([]byte)

	// Content must provide the File Content when content is
	// changed or when the File is Mound-ed.
	Content() []byte
}

// ParentAndName strip basename and filename from a given path.
func ParentAndName(path string) (string, string) {
	parts := strings.Split(path, s)
	name := parts[len(parts)-1]
	parent := strings.Join(parts[:len(parts)-1], s)
	return parent, name
}
