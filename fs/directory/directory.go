package directory

import (
	"errors"
	"fmt"
	"os"
	"sort"

	nfs "git.framacode.org/floreal/mitosis/fs"
	"git.framacode.org/floreal/mitosis/fs/file"
)

// directory is an implementation of
// git.framacode.org/floreal/mitosis/fs/Directory
// and will respect it's behavior requirement
type directory struct {
	parent    string
	name      string
	children  []nfs.Entry
	isMounted bool
}

// Mount attempts to open path as a directory,
// reed it's content and stores it entries into
// children.
// Mount also sets isMounted to true.
// error may be returned upon failure.
func (d *directory) Mount() error {
	f, err := os.Open(d.Path())
	if err != nil {
		return err
	}
	defer f.Close()
	cs, err := f.ReadDir(-1)
	if err != nil {
		return err
	}
	for _, c := range cs {
		var e nfs.Entry
		path := fmt.Sprintf(`%s/%s/%s`, d.parent, d.name, c.Name())
		switch {
		case c.Type().IsDir():
			e = New(path)
		case c.Type().IsRegular():
			e = file.New(path)
		}
		d.children = append(d.children, e)
	}
	d.isMounted = true
	return nil
}

// UnMount will UnMount each children, they
// won't be Persist-ed and set isMounted to
// false.
func (d *directory) UnMount() {
	if !d.isMounted {
		return
	}
	for _, c := range d.children {
		c.UnMount()
	}
	d.isMounted = false
	d.children = []nfs.Entry{}
}

func (d *directory) Name() string {
	return d.name
}

func (d *directory) Path() string {
	return fmt.Sprintf(`%s/%s`, d.parent, d.name)
}

// AddChild adds a subdirectory or a file to the curent directory
// see git.framacode.org/floreal/mitosis/fs/Directory for further
// details.
func (d *directory) AddChild(name string, entryType nfs.EntryType) (nfs.Entry, error) {
	if !d.isMounted {
		return nil, errors.New(fmt.Sprintf(`Can't Add Child "%s" to "%s", Directory not mounted`, name, d.Path()))
	}
	if len(name) == 0 {
		return nil, errors.New(`No name given to child`)
	}

	path := fmt.Sprintf("%s/%s/%s", d.parent, d.name, name)
	var e nfs.Entry = nil
	switch entryType {
	case nfs.DirectoryEntry:
		e = New(path)
	case nfs.FileEntry:
		e = file.New(path)
	default:
		return nil, errors.New(fmt.Sprintf(`Invalid Entry Type "%s"`, entryType))
	}
	d.children = append(d.children, e)

	sort.Slice(d.children, func(i, j int) bool {
		return d.children[i].Name() < d.children[j].Name()
	})

	return e, nil
}

// Persist creates the curent directory's path
// see git.framacode.org/floreal/mitosis/fs/Directory for further
// details.
func (d *directory) Persist() error {
	if err := os.Mkdir(d.Path(), 0700); err != nil && !os.IsExist(err) {
		return err
	}
	// At this point file already exists
	fstats, err := os.Stat(d.Path())
	if err != nil {
		return err
	}
	if !fstats.IsDir() {
		return errors.New(fmt.Sprintf(`File "%s" already exists and is not a Directory`, d.Path()))
	}
	return nil
}

// Children provides the children list
// see git.framacode.org/floreal/mitosis/fs/Directory for further
// details.
func (d *directory) Children() ([]nfs.Entry, error) {
	if !d.isMounted {
		return nil, errors.New(fmt.Sprintf(`Can't provide "%s"'s children list , Directory not mounted`, d.Path()))
	}
	return d.children, nil
}

// New Creates a directory struct instance and returns a pointer
// to it.
func New(path string) nfs.Directory {
	parent, name := nfs.ParentAndName(path)
	return &directory{
		parent:    parent,
		name:      name,
		children:  []nfs.Entry{},
		isMounted: false,
	}
}
