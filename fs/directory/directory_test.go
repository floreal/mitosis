package directory_test

import (
	"fmt"
	"os"
	"testing"

	"git.framacode.org/floreal/mitosis/fs"
	"git.framacode.org/floreal/mitosis/fs/directory"
	"git.framacode.org/floreal/mitosis/fs/file"
	"github.com/stretchr/testify/assert"
)

func TestNew(t *testing.T) {
	a := assert.New(t)

	dirname := "/tmp/test/root"
	e := directory.New(dirname)
	a.Equal("/tmp/test/root", e.Path())
	a.Equal("root", e.Name())
}

func TestDirectory_Persist(t *testing.T) {
	a := assert.New(t)

	dirname, e := createTmpRandomDir()
	defer os.RemoveAll(dirname)
	err := e.Persist()
	a.Nil(err)
}

func TestDirectory_Persist_Fails(t *testing.T) {
	a := assert.New(t)

	filename, e := createTmpRandomDir()
	os.RemoveAll(filename)
	f, _ := os.Create(filename)
	f.Close()
	defer os.Remove(filename)

	err := e.Persist()
	a.EqualError(err, fmt.Sprintf(`File "%s" already exists and is not a Directory`, filename))
}

func TestDirectory_Mount_And_Children(t *testing.T) {
	a := assert.New(t)

	dirname, _ := os.MkdirTemp("", "test*")
	defer os.RemoveAll(dirname)

	sdname := fmt.Sprintf(`%s/%s`, dirname, `subdir`)
	sfname := fmt.Sprintf(`%s/%s`, dirname, `subfile`)

	os.Mkdir(sdname, 0700)
	f, _ := os.Create(sfname)
	f.Close()

	e := directory.New(dirname)
	err := e.Mount()
	a.Nil(err)
	entries, err := e.Children()
	a.Nil(err)

	a.Contains(entries, directory.New(sdname))
	a.Contains(entries, file.New(sfname))
}

func TestDirectory_Children_FailsWhenNotMounted(t *testing.T) {
	a := assert.New(t)

	dirname := "/tmp/test/root"
	e := directory.New(dirname)
	enries, err := e.Children()
	a.EqualError(err, `Can't provide "/tmp/test/root"'s children list , Directory not mounted`)
	a.Nil(enries)
}

func TestDirectory_Mount_Fails(t *testing.T) {
	a := assert.New(t)

	dirname := "/tmp/test/root"
	e := directory.New(dirname)
	err := e.Mount()
	a.EqualError(err, `open /tmp/test/root: no such file or directory`)
}

func TestDirectory_Mount_FailsWhenPathIsRegularFile(t *testing.T) {
	a := assert.New(t)

	dirname, _ := os.MkdirTemp("", "test*")
	defer os.RemoveAll(dirname)

	sfname := fmt.Sprintf(`%s/%s`, dirname, `subfile`)
	f, _ := os.Create(sfname)
	f.Close()
	e := directory.New(sfname)
	a.EqualError(e.Mount(), fmt.Sprintf(`readdirent %s/subfile: not a directory`, dirname))
}

func TestDirectory_Unmount(t *testing.T) {
	a := assert.New(t)

	dirname, _ := os.MkdirTemp("", "test*")
	defer os.RemoveAll(dirname)

	sdname := fmt.Sprintf(`%s/%s`, dirname, `subdir`)
	sfname := fmt.Sprintf(`%s/%s`, dirname, `subfile`)

	e := directory.New(dirname)
	e.Mount()

	sde, _ := e.AddChild(`subdir`, fs.DirectoryEntry)
	sfe, _ := e.AddChild(`subfile`, fs.FileEntry)

	sde.Persist()
	sde.Mount()
	sfe.Persist()
	sfe.Mount()

	expectedSde := directory.New(sdname)
	expectedSfe := file.New(sfname)
	a.NotEqual(expectedSde, sde)
	a.NotEqual(expectedSfe, sfe)

	e.UnMount()
	a.Equal(expectedSde, sde)
	a.Equal(expectedSfe, sfe)

	e.UnMount()
}

func TestDirectory_AddChild(t *testing.T) {
	a := assert.New(t)

	dirname, e := createTmpRandomDir()
	defer os.RemoveAll(dirname)
	_ = e.Mount()
	subdir, err := e.AddChild(`child_dir`, fs.DirectoryEntry)
	a.Nil(err)
	subfile, err := e.AddChild(`file`, fs.FileEntry)
	a.Nil(err)
	a.Equal(directory.New(fmt.Sprintf("%s/%s", dirname, `child_dir`)), subdir)
	a.Equal(file.New(fmt.Sprintf("%s/%s", dirname, `file`)), subfile)
}

func TestDirectory_AddChild_FailsWhenNotMounted(t *testing.T) {
	a := assert.New(t)

	dirname := "/tmp/test/root"
	e := directory.New(dirname)
	se, err := e.AddChild(`subdir`, fs.FileEntry)
	a.EqualError(err, `Can't Add Child "subdir" to "/tmp/test/root", Directory not mounted`)
	a.Nil(se)
}

func TestDirectory_AddChild_FailsWhenBadEntryTypeIsProvided(t *testing.T) {
	a := assert.New(t)

	dirname, e := createTmpRandomDir()
	defer os.RemoveAll(dirname)
	_ = e.Mount()
	se, err := e.AddChild(`subdir`, fs.EntryType(127))
	a.EqualError(err, `Invalid Entry Type "EntryType(127)"`)
	a.Nil(se)
}

func TestDirectory_AddChild_FailsWhenNameIsEmpty(t *testing.T) {
	a := assert.New(t)

	dirname, e := createTmpRandomDir()
	defer os.RemoveAll(dirname)
	_ = e.Mount()
	se, err := e.AddChild(``, fs.FileEntry)
	a.EqualError(err, `No name given to child`)
	a.Nil(se)

}

func createTmpRandomDir() (string, fs.Directory) {
	dirname, _ := os.MkdirTemp("", "test*")
	e := directory.New(dirname)
	return dirname, e
}
