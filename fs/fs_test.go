package fs_test

import (
	"testing"

	"git.framacode.org/floreal/mitosis/fs"
	"github.com/stretchr/testify/assert"
)

func TestParentAndName(t *testing.T) {
	a := assert.New(t)

	p, n := fs.ParentAndName("/tmp/dir")

	a.Equal("/tmp", p)
	a.Equal("dir", n)
}
